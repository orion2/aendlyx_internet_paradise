FROM node:alpine
RUN echo "[aex-0.0.6]"

#RUN apk add --no-cache build-base gcc autoconf automake libtool zlib-dev libpng-dev nasm

WORKDIR /usr/grids
COPY . .

# RUN yarn install
CMD ["yarn","run","serve"]