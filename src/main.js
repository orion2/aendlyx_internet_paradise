
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './pages/root/store.js'
import './registerServiceWorker'
Vue.config.productionTip = false
import 'vue-resize/dist/vue-resize.css'
import VueResize from 'vue-resize'
Vue.use(VueResize)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#webx')

